{- compute a map from starting Pos of an occurrence of a variable
   to the Var for the introduction of that variable. -}
module ScopeMap(scopeMap, ScopeMap, scope, scopeVar, bindingIf, bindingIfTy, bindingIfConstraint) where

import Pos
import Syntax
import Trie
import Util
import Constraint
import qualified Data.Map as M
import Data.Foldable
import Control.Applicative hiding (Const)
import Control.Monad
import Control.Monad.Reader
import Control.Monad.State.Lazy

-- map positions of occurrences of variables to the Vars introducing them
type ScopeMap = M.Map Pos Var

scopeMap :: FileWithDeps -> ScopeMap
scopeMap (deps,mainfile) =
  M.fromList $
  evalState (mappingsInSimpleFile
             (join ((snd <$> (join (postfix <$> deps))) ++ [mainfile])))
            (trieEmpty,trieEmpty)

scope :: Pos -> ScopeMap -> Maybe Pos
scope pos sm = varPos <$> scopeVar pos sm

scopeVar :: Pos -> ScopeMap -> Maybe Var
scopeVar pos sm =
  do
    (pos',v) <- M.lookupLE pos sm 
    {- pos' is the starting position of the variable occurrence, call it q,
       closest to the left of pos -}
    let n = length (varStr v) 
    if pos >= ((+ n) <$> pos') then
       -- the requested position is outside q
       Nothing
    else
       Just v
 
-- return the binding occurrence of the given variable, if it has one
bindingIf :: ScopeMap -> Var -> Var
bindingIf sm v =
  case scopeVar (varPos v) sm of
    Nothing -> v
    Just v' -> v'    
  
-- replace all Consts in the given Ty with their binding occurrences
bindingIfTy :: ScopeMap -> Ty -> Ty
bindingIfTy sm (Arrow ps arr ty1 ty2) = Arrow ps arr (bindingIfTy sm ty1) (bindingIfTy sm ty2)
bindingIfTy sm (TApp(c,tys)) = TApp (bindingIf sm c, bindingIfTy sm <$> tys)
bindingIfTy sm (TAppMeta(m,tys)) = TAppMeta(m,bindingIfTy sm <$> tys)
bindingIfTy sm (TyParens ps ty) = TyParens ps (bindingIfTy sm ty)

bindingIfConstraint :: ScopeMap -> Constraint -> Constraint
bindingIfConstraint sm (Subtype ty1 ty2) = Subtype (bindingIfTy sm ty1) (bindingIfTy sm ty2)
bindingIfConstraint sm (Like ty1 ty2) = Like (bindingIfTy sm ty1) (bindingIfTy sm ty2)

----------------------------------------------------------------------
-- type definitions Mappings and MappingsMod
--
-- To compute a ScopeMap for a File, we use a pair
-- of PosMaps to map variable names in scope, either term or type
-- variables, to positions where they are introduced.  Such a pair
-- we call a Context.
----------------------------------------------------------------------

-- for maps from variable names to Vars for their introduction 
type PosMap = Trie Char Var
type Context = (PosMap,PosMap) -- one for type-level, the other for term-level

data WhichVar = TmWhich | TyWhich

selectPosMap :: WhichVar -> Context -> PosMap
selectPosMap TmWhich = fst
selectPosMap TyWhich = snd

{- type schemes for the various recursive functions below

   We compute MappingLists which will then be converted into
   a ScopeMap by computeScopeMap above.

   Mappings reads and extends a Context as it goes under bindings.

   MappingsMod actually modifies a Context.  It is used for the top-level
   Statements, which all introduce new symbols.  We need to add mappings for
   those symbols in subsequent Statements. This could also just be
   done with the Reader monad, but this worked out fine and I do not
   want to refactor it at present. -}
type MappingList = [(Pos,Var)]
type MappingsReader = Reader Context MappingList
type Mappings a = a -> MappingsReader
type MappingsMod a = a -> State Context MappingList

----------------------------------------------------------------------
-- combinators for lifting Mappings/MappingsMod through type ctors
----------------------------------------------------------------------

----------------------------------------------------------------------
-- functions related to variables 
----------------------------------------------------------------------

posMapAdd :: Var -> PosMap -> PosMap
posMapAdd v pm = trieInsert (varStr v) v pm

ctxtAdd :: WhichVar -> Var -> Context -> Context
ctxtAdd TmWhich v (m1,m2) = (posMapAdd v m1,m2)
ctxtAdd TyWhich v (m1,m2) = (m1,posMapAdd v m2)

{- Produce a mapping for a binding occurrence of a constant.
   This also adds the constant to the context. -}
mappingsInBindingConst :: WhichVar -> MappingsMod Const
mappingsInBindingConst which v = 
  do
    ctxt <- get
    put $ ctxtAdd which v ctxt
    return [(varPos v, v)]


mappingsInVar :: WhichVar -> Mappings Var 
mappingsInVar which v =
  do
    ctxt <- ask
    case trieLookup (varStr v) (selectPosMap which ctxt) of
      Nothing -> return []
      Just v' -> return [(varPos v,v')] -- map the position of this occurrence to the introducing Var
mappingsInConst = mappingsInVar

addParams :: WhichVar -> [Var] -> MappingsReader -> MappingsReader
addParams w params r =
  aseq [local (\ c -> foldr (ctxtAdd w) c params) r,
        -- add mappings for the binding occurrences of the params
        return (map (\ v -> (varPos v,v)) params)]

addTyParams :: [Var] -> MappingsReader -> MappingsReader
addTyParams = addParams TyWhich

addTmParams :: [Var] -> MappingsReader -> MappingsReader
addTmParams = addParams TmWhich

-- add type parameters to term definitions, which have two cases
addTyParamsTm :: [TyParamTm] -> MappingsReader -> MappingsReader
addTyParamsTm [] r = r
addTyParamsTm (TyParam v : params) r = addTyParam v params r
addTyParamsTm (TyParamLike _ v ty : params) r = 
  aseq [ mappingsInTy ty, addTyParam v params r]    

-- factors out adding a variable and recursing on remaining params, from the two cases for addTyParamsTm
addTyParam :: Var -> [TyParamTm] -> MappingsReader -> MappingsReader
addTyParam v params r = 
  local (ctxtAdd TyWhich v) $
  addTyParamsTm params r

----------------------------------------------------------------------
-- recursive functions through the syntax
----------------------------------------------------------------------

-- mappings a position in a SimpleFile
mappingsInSimpleFile :: MappingsMod SimpleFile
mappingsInSimpleFile = concatMapM mappingsInStatement 

{- we skip this statement if the position we are looking for is
   before the start of the statement -}
mappingsInStatement :: MappingsMod Statement
mappingsInStatement (TyDefSt d)   = mappingsInTyDef d
mappingsInStatement (DataDefSt d) = mappingsInDataDef d
mappingsInStatement (TmDefSt d)   = mappingsInTmDef d

mappingsInTyDef :: MappingsMod TyDef
mappingsInTyDef (TyDef _ c params ty) =
  aseq [rd (addTyParams params (mappingsInTy ty)),
         mappingsInBindingConst TyWhich c]

mappingsInDataDef :: MappingsMod DataDef
mappingsInDataDef (DataDef _ c params ctors) =
  aseq [mappingsInBindingConst TyWhich c,
        concatMapM (mappingsInCtrDef params) ctors ]

{- we take in the type params of the containing DataDef,
   so we can add them just using the Reader (not with the State monad) -}
mappingsInCtrDef :: [Var] -> MappingsMod CtrDef
mappingsInCtrDef params (c,tys) =
  aseq [rd (addTyParams params (concatMapM mappingsInTy tys)),
         mappingsInBindingConst TmWhich c]
    
mappingsInTmDef :: MappingsMod TmDef
mappingsInTmDef (TmDef _ c params mty tm) =
  aseq[rd (addTyParamsTm params
            $ aseq [ maybeMapM mappingsInTy mty ,
                     mappingsInTm tm]),
        mappingsInBindingConst TmWhich c]

mappingsInTm :: Mappings Tm
mappingsInTm (Var v) = mappingsInVar TmWhich v
mappingsInTm (Lam _ vs t) =
  addTmParams vs
    $ aseq [concatMapM (mappingsInVar TmWhich) vs, mappingsInTm t]
mappingsInTm (App t1 t2) =
  aseq [mappingsInTm t1, mappingsInTm t2]
mappingsInTm (Parens _ t) = mappingsInTm t
mappingsInTm (Omega _ f x funct body) =
  addTmParams [f,x]
   $ aseq [mappingsInVar TmWhich f , mappingsInVar TmWhich x ,
                  mappingsInTApp funct, mappingsInTm body]
mappingsInTm (Gamma _ tm cs) = aseq [mappingsInTm tm , concatMapM mappingsInCase cs]
mappingsInTm (Sigma _ bs tm) =
  mappingsInSigmaBindings (mappingsInTm tm) bs 

mappingsInCase :: Mappings Case
mappingsInCase (Case p t) =
  addTmParams (getPatVars p)
    $ aseq [mappingsInPat p, mappingsInTm t]

mappingsInSigmaBindings :: MappingsReader -> Mappings [SigmaBinding]
mappingsInSigmaBindings r [] = r
mappingsInSigmaBindings r (SigmaBinding _ x t : bs) =
  aseq [ mappingsInTm t ,
         addTmParams [x] $ mappingsInSigmaBindings r bs ]

getPatVars :: Pat -> [Var]
getPatVars (Pat _ vs) = vs

mappingsInPat :: Mappings Pat
mappingsInPat (Pat c vs) =
  aseq [mappingsInConst TmWhich c,
               concatMapM (mappingsInVar TmWhich) vs]

mappingsInTy :: Mappings Ty
mappingsInTy (TAppMeta _) = return [] -- no mappings in meta-variables
mappingsInTy (Arrow _ _ t1 t2) = aseq [ mappingsInTy t1 , mappingsInTy t2]
mappingsInTy (TApp ta) = mappingsInTApp ta
mappingsInTy (TyParens _ t) = mappingsInTy t

mappingsInTApp :: Mappings TApp
mappingsInTApp (c,tys) =
  aseq [mappingsInConst TyWhich c , concatMapM mappingsInTy tys]

