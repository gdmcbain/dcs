{-# LANGUAGE TupleSections #-}

{- renaming of MetaVars and Consts -}

module Renaming where

import Pos
import ScopeMap
import Syntax
import SyntaxHelpers
import SubstTy
import Constraint
import Control.Monad.State.Lazy
import qualified Data.Map as M
import Util
import SubstTy

type Renaming a b = M.Map a b

type FullRenaming = (Renaming Const Const, Renaming MetaVar Const, Renaming MetaVar MetaVar )

-- this maps starting positions of statements to renamings to apply 
-- within those statements
type RenamingPer = M.Map Pos FullRenaming

renamingToMetaTySubst :: (b -> Ty) -> Renaming MetaVar b -> MetaTySubst
renamingToMetaTySubst = M.map

showRenamingBy :: (a -> String) -> (b -> String) -> Renaming a b -> String
showRenamingBy s1 s2 r =
  M.foldrWithKey (\ src tgt str -> "\n" ++ s1 src ++ " -> " ++ s2 tgt ++ str) "" r

-- just show first maps of the renaming pairs
showFullRenaming :: FullRenaming -> String
showFullRenaming (rc,rm1,rm2) = 
  "Consts:" ++ showRenamingBy (\v -> "  " ++ showVarDetails v) showVar rc ++
  "\nMetaVars to Consts:" ++ showRenamingBy (\v -> "  " ++ showMetaVarDetails v) showConst rm1 ++ 
  "\nMetaVars:" ++ showRenamingBy (\v -> "  " ++ showMetaVarDetails v) showMetaVar rm2 ++ "\n"

showRenamingPer :: RenamingPer -> String
showRenamingPer r =
  M.foldr (\ fr str -> "-------------------------------\n" ++ showFullRenaming fr ++ str) "" r

-- we are going to compute a mapping for the names of symbols to a unique identifying number,
-- starting from 0 and incremented
type StringCount = M.Map String Int

type RenameSt = State StringCount String

renameStr :: String -> RenameSt
renameStr s =
  do
    renamest <- get
    case M.lookup s renamest of
      Nothing ->
        do
          put $ M.insert s 1 renamest
          return $ s ++ "₀"
      Just x -> 
        do
          put $ M.insert s (x + 1) renamest
          return $ s ++ (toUnicodeSubscript x)

type Rename a b = [a] -> State StringCount [b]

renamel :: Rename String String
renamel ss = mapM renameStr ss

renameMetaVars :: Rename MetaVar MetaVar
renameMetaVars ms = ((,noExtent) <$>) <$> (renamel (metaVarStr <$> ms))

renameMetaVarsToConsts :: Rename MetaVar Const
renameMetaVarsToConsts ms = ((,noPos) <$>) <$> (renamel (metaVarStr <$> ms))

renameConsts :: Rename Const Const
renameConsts ms = ((,noPos) <$>) <$> (renamel (constStr <$> ms))

mkRenaming :: Ord a => [a] -> [b] -> Renaming a b
mkRenaming xs ys = foldr (\ (x,y) m -> M.insert x y m) M.empty (zip xs ys)


-- convert ms1 to constants and ms2 to metavars
renamingFull :: [Const] -> [MetaVar] -> [MetaVar] -> FullRenaming
renamingFull cs ms1 ms2 =
  let (cs',cs1',ms2') = evalState (do
                                      cs' <- renameConsts cs
                                      ms1' <- renameMetaVarsToConsts ms1
                                      ms2' <- renameMetaVars ms2
                                      return (cs',ms1',ms2')) M.empty
  in
    (mkRenaming cs cs',mkRenaming ms1 cs1',mkRenaming ms2 ms2')

renameConst :: ScopeMap -> FullRenaming -> Const -> Const
renameConst sm (rc,_,_) c = 
  case M.lookup (bindingIf sm c) rc of
    Nothing -> c
    Just c' -> c'

-- rename metavars and arity-0 consts, using the given FullRenaming
renameTy :: ScopeMap -> FullRenaming -> Ty -> Ty
renameTy sm r@(_,rm1,rm2) ty@(TAppMeta (m,tys)) =
  let tys' = renameTy sm r <$> tys in
    case M.lookup m rm1 of
      Nothing ->
        TAppMeta(case M.lookup m rm2 of
                   Nothing -> m
                   Just m' -> m',
                 tys')
      Just c' -> TApp(c', tys')
renameTy sm r@(rc,_,_) ty@(TApp (c,[])) =
  tyConst $ renameConst sm r c
renameTy sm r ty@(TApp (c,tys)) = TApp(c,renameTy sm r <$> tys)
renameTy sm r (Arrow _ arr t1 t2) = Arrow [] arr (renameTy sm r t1) (renameTy sm r t2)
renameTy sm r (TyParens _ ty) = renameTy sm r ty

renameConstPer :: ScopeMap -> RenamingPer -> Pos -> Const -> Const
renameConstPer sm r pos c =
  case M.lookupLE pos r of
    Nothing -> c
    Just (_,fullr) ->
      renameConst sm fullr c


getFullRenaming :: RenamingPer -> Pos -> Maybe FullRenaming
getFullRenaming r pos =
  do
    (_,fullr) <- M.lookupLE pos r 
    return fullr
  
renameTyPer :: ScopeMap -> RenamingPer -> Pos -> Ty -> Ty
renameTyPer sm r pos ty =
  case getFullRenaming r pos of
    Nothing -> ty
    Just fullr ->
      renameTy sm fullr ty

renameTyParamTmPer :: ScopeMap -> RenamingPer -> Pos -> TyParamTm -> TyParamTm
renameTyParamTmPer sm r pos (TyParam v) = TyParam (renameConstPer sm r pos v)
renameTyParamTmPer sm r pos (TyParamLike ps v ty) = TyParamLike ps (renameConstPer sm r pos v) (renameTyPer sm r pos ty)

renameConstraintPer :: ScopeMap -> RenamingPer -> Pos -> Constraint -> Constraint
renameConstraintPer sm ren pos (Subtype ty1 ty2) = Subtype (renameTyPer sm ren pos ty1) (renameTyPer sm ren pos ty2)
renameConstraintPer sm ren pos (Like ty1 ty2) = Like (renameTyPer sm ren pos ty1) (renameTyPer sm ren pos ty2)