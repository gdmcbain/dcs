{-# LANGUAGE TupleSections #-}

{- check entailment between sets of subtyping constraints,
   producing a type substitution for solutions of meta-variables -}
module Solver(solve,solveStep) where

import Pos
import Syntax
import SyntaxHelpers
import Constraint
import TyDefs
import Variances(VariancesMap)
import Control.Monad
import Control.Monad.State.Lazy
import qualified Data.Map as M
import SubstTy
import ScopeMap
import Variances
import Util
import Data.List

type Solve a = State MetaTySubst a

{- given a list of assumed subtyping contraints, and
   another list of required subtyping constraints, with their extents, 
   produce a type substitution for meta-variables that we solved for.
   Also return the updated assumed constraints (we might add assumptions
   as we go, or apply a substitution to them).  Finally,
   return required subtyping constraints that do not follow from
   the assumed ones.

   This function just loops through the constraints repeatedly using
   solveStep until no change.

   We use the VariancesMap both to propagate subtyping constraints correctly,
   and also to check which type constants are global (should be mapped by
   the VariancesMap) and which are local (not mapped).
-}
solve :: ScopeMap -> TyDefs -> VariancesMap -> [Constraint] -> [EConstraint] -> Solve ([Constraint],[EConstraint])
solve sm tydefs vsm assumed required =
  fixM (solveStep sm tydefs vsm) (saturate assumed, required)

-- add consequences of constraints, in particular of Like constraints
saturate :: [Constraint] -> [Constraint]
saturate = foldr h []
  where h c@(Like ty1 ty2) r =             -- R ~ Nat
          (Subtype ty1 (tapp ty2 [ty1])) : -- R <: Nat R
          (Subtype ty1 ty2) :              -- R <: Nat (as a datatype)
          c : r
        h c r = c : r

-- perform a single step of solving using solvePass, then apply the current substitution to the assumed
-- constraints.
solveStep :: ScopeMap -> TyDefs -> VariancesMap -> ([Constraint] , [EConstraint]) -> Solve ([Constraint],[EConstraint])
solveStep sm tydefs vsm (assumed, required) =
  do
    (assumed',unsolved) <- solvePass sm tydefs vsm assumed required
    subst <- get
    return (substConstraint subst <$> (assumed ++ assumed'), unsolved)

-- return new assumptions as well as unsolved constraints
solvePass :: ScopeMap -> TyDefs -> VariancesMap -> [Constraint] -> [EConstraint] -> Solve ([Constraint],[EConstraint])
solvePass sm tydefs vsm assumed [] = return ([],[]) -- no new assumptions, nothing to prove
solvePass sm tydefs vsm assumed ((ext,s):toprove) =
  do
    subst <- get
    case decompose sm tydefs vsm subst assumed s of
      (assumed',[]) ->
        -- decomposition completely eliminated a constraint
        do
          (assumed'',unsolved) <- solvePass sm tydefs vsm assumed toprove
          return (assumed' ++ assumed'',unsolved)
      (assumed',s':r) -> 
        -- we broke a constraint down
        do
          (assumed1,unsolved1) <- solveOne sm tydefs vsm assumed ext s'
          (assumed2,unsolved2) <- solvePass sm tydefs vsm assumed (((ext,) <$> r) ++ toprove)
          return (assumed'++assumed1++assumed2,unsolved1 ++ unsolved2)

{- unfold definition of c and instantiate its params with tys; params missing arguments will
   be instantiated with fresh meta-vars.  Also, replace constant with its binding occurrence if
   it was not defined, for convenience later. -}
unfoldDefIf :: ScopeMap -> TyDefs -> MetaTySubst -> Const -> [Ty] -> Ty
unfoldDefIf sm tydefs subst c tys =
  let c' = bindingIf sm c in
    case M.lookup (constPos c') tydefs of
      Nothing -> TApp(c',tys)
      Just (params,ty) -> 
        -- metasubst maps extra parameters to fresh metavars
        let metasubst = paramMetaSubst (extentConst c) (drop (length tys) params)
            s = foldr (\ (param,ty) -> M.insert (constPos param) ty) metasubst (zip params tys) in

        -- s now maps params to corresponding arguments, and any extra params to fresh meta-vars

        -- apply the substitution to instantiate the definition, and then recurse (for further defs to unfold)
          prepareTopLevel sm tydefs subst (substTy sm s ty)

substMetaIf :: ScopeMap -> TyDefs -> MetaTySubst -> MetaVar -> [Ty] -> Ty
substMetaIf sm tydefs subst m tys = 
  case M.lookup m subst of
    Nothing -> TAppMeta(m,tys)
    Just ty -> prepareTopLevel sm tydefs subst (tapp ty tys)

-- apply the current substitution, and unfold definitions at the top-level only of the given Ty
prepareTopLevel :: ScopeMap -> TyDefs -> MetaTySubst -> Ty -> Ty
prepareTopLevel sm tydefs subst ty = h $ substTyMeta subst ty
  where h (TApp (c,tys)) = unfoldDefIf sm tydefs subst c tys
        h ty = ty
      
propagate :: [Variance] -> [Ty] -> [Ty] -> [Constraint]
propagate vs args args' =
  join $ map (\ (v,a,a') ->
                 let c1 = Subtype a a'
                     c2 = Subtype a' a in
                   case v of
                     Positive -> [c1]
                     Negative -> [c2]
                     Mixed -> [c1,c2] -- the position is invariant, so a must equal a'
                     Unused -> [])
  (zipWith3 (,,) vs args args')


findLike :: Const -> [Constraint] -> Maybe Ty
findLike c cs = (\ (Like _ ty) -> ty) <$> find likeC cs
  where likeC (Like (TApp(c',[])) _) | c == c' = True
        likeC _ = False

{- decompose the given subtyping constraint if possible, applying the substitution as we go,
   and looking up defined type constants using the TyDefs.

   Return new constaints that should be assumed, and any constraints left after decomposition.
-}
decompose :: ScopeMap -> TyDefs -> VariancesMap -> MetaTySubst ->
             [Constraint] -> Constraint -> ([Constraint],[Constraint])
decompose sm tydefs vsm subst assumed c =
  case c of
    (Subtype ty1 ty2) -> decomposeSubtype (h ty1) (h ty2)
    (Like ty1 ty2) -> decomposeLike (h ty1) (h ty2)
  where h = prepareTopLevel sm tydefs subst

        decomposeL l = let (x,y) = unzip $ decompose sm tydefs vsm subst assumed <$> l in (join x, join y)
        propagateArgs c args args' = 

            -- propagate subtyping to args and args'
            case M.lookup (constPos c) vsm of
              Nothing -> ([],[]) {- stuck without variances; error should get reported via kinding -}
              Just vs -> decomposeL $ propagate vs args args'

        decomposeSubtype :: Ty -> Ty -> ([Constraint],[Constraint])

        -- A. compare arrow types

        -- A.1 both plain arrows
        decomposeSubtype (Arrow _ ArrowPlain ty1a ty1b) (Arrow _ ArrowPlain ty2a ty2b) =
          decomposeL [Subtype ty2a ty1a , Subtype ty1b ty2b]

        -- A.2 both fat arrows
        -- introduce a new constant and then compare applications to that constant as if plain arrows
        decomposeSubtype ty1@(Arrow _ ArrowFat ty1a ty1b) ty2@(Arrow _ ArrowFat ty2a ty2b) =
          -- UNSOUND: this use of maxConsts needs to be replaced with use of a threaded supply of fresh consts
          let m = maxConsts [ty1,ty2]
              f1 = tyConst ("C",m)
              f2 = tyConst ("C",(+ 1) <$> m) 
              f3 = tyConst ("C",(+ 2) <$> m) in -- these constants need to be globally fresh!  This just gives local freshness
            decomposeL [Subtype (tapp ty2a [f1]) (tapp ty1a [f1]) , Subtype (tapp ty1b [f2,f3]) (tapp ty2b [f2,f3])]

        -- A.3 fat arrow <: plain arrow with domain parts headed by same constant
        decomposeSubtype ty1@(Arrow _ ArrowFat ty1a@(TApp(c,tys)) ty)
                         ty2@(Arrow _ ArrowPlain ty1b@(TApp(c',tys')) ty') | c == c' =
          decomposeL [Subtype ty1b ty1a, Subtype (tapp ty [ty1b,ty1b]) ty']

        -- A.4 looks like   D => C  <:  R -> T,
        --     where R ~ D' is assumed
        decomposeSubtype ty1@(Arrow _ ArrowFat ty1a ty1b) ty2@(Arrow _ ArrowPlain ty2a@(TApp(c,[])) ty') =
          case findLike c assumed of
            Nothing  -> ([],[Subtype ty1 ty2])
            Just ty'' -> decomposeL [Subtype ty'' ty1a, Subtype (tapp ty1b [ty2a,ty2a]) ty']

        -- A.5 looks like   D => C  <: X -> T
        --     where X is a meta-variable
        
        decomposeSubtype ty1@(Arrow _ ArrowFat ty1a ty1b) ty2@(Arrow _ ArrowPlain ty2a@(TAppMeta(m,[])) ty') =
          let (assumed',remaining) = decompose sm tydefs vsm subst assumed (Subtype (tapp ty1b [ty2a,ty2a]) ty') in
            (Like ty2a ty1a : assumed', remaining)

        -- B. compare datatype or signature functor applications 
        -- (prepareTopLevel already gives us binding occurrences for c and c')

        decomposeSubtype ty1@(TApp(c,args)) ty2@(TApp(c',args'))

          -- B.1 datatypes are the same, and have same number of args
          | c == c' && length args == length args' =
            propagateArgs c args args'
        
        -- B.2 datatypes are the same, but second arg list is 1 longer than first
        -- swap, to go to B.3
          | c == c' && length args' == length args + 1 = 
            decomposeSubtype ty2 ty1 

        -- B.3 datatypes same, first arg list is 1 longer
        -- This means that ty1 is a signature functor application, and ty2 is just the datatype application
        -- We just unroll the datatype application and recurse, as this will then hit case B.1 above
          | c == c' && length args == length args' + 1 = 
            decomposeSubtype ty1 (tapp ty2 [ty2])

        -- C. could not decompose the constraint
        decomposeSubtype ty1 ty2 = ([],[Subtype ty1 ty2])

        decomposeLike :: Ty -> Ty -> ([Constraint],[Constraint])
        decomposeLike ty1@(TApp(c,tys)) ty2@(TApp(c',tys')) | c == c' && (length tys == length tys') =
          propagateArgs c tys tys'
        decomposeLike ty1@(TApp(c,tys)) ty2@(TApp(c',tys')) | c == c' && (length tys == length tys' + 1) =
          let (x,y) = propagateArgs c tys tys' 
              (x',y') = decomposeLike (last tys) ty2 in
            (x++x',y++y')

        decomposeLike ty1 ty2 = ([],[Like ty1 ty2])
            
type IsLocal = Const -> Bool

violatesScoping :: IsLocal -> MetaVar -> Ty -> Bool
violatesScoping islocal m ty =
  let (sp,_) = extentMetaVar m
      cs = constPos <$> filter islocal (consts ty) in
    any (>= sp) cs

-- this assumes that the meta-variable identified by (x,e) is not solved yet in the subst
-- the returned Bool is False iff the assignment could not be made because either
--   i.  the metavar occurs in Ty
--   ii. the metavar is introduced outside the scope of a constant occurring in the Ty
addSolution :: IsLocal -> MetaVar -> Ty -> Solve Bool
addSolution islocal m (TAppMeta(m',[])) | m == m' = return True -- special case where a variable is mapped to itself
addSolution islocal m ty =
  if occursIn m ty || violatesScoping islocal m ty then
    return False
  else
    do
      subst <- get
      put $ M.insert m (substTyMeta subst ty) subst
      return True

-- try to add a solution, but if that fails, return the given EConstraint.  The first list
-- of Constraints is always empty
addSolutionDefault :: IsLocal -> MetaVar -> Ty -> Extent -> Constraint -> Solve ([Constraint],[EConstraint])
addSolutionDefault islocal m ty ext s =
  do
    b <- addSolution islocal m ty
    return (if b then ([],[])
            else ([],[(ext,s)]))

{- at this point, the types ty1 and ty2 should not be arrow types, and they should not be applications
   of the same datatype (as that has been handled already in decompose).

   In general, all
   meta-variables coming from Infer should have arity either 0 or 1 (nothing more).

   Like several other functions in this module, return new assumed constraints as the first returned
   component.
-}
solveOne :: ScopeMap -> TyDefs -> VariancesMap -> [Constraint] -> Extent ->
            Constraint -> Solve ([Constraint],[EConstraint])
solveOne _ _ vsm _ ext s@(Subtype (TAppMeta(m,[])) ty2) =
  addSolutionDefault (isLocal vsm) m ty2 ext s
solveOne _ _ vsm _ ext s@(Subtype ty1 (TAppMeta(m,[]))) =
  addSolutionDefault (isLocal vsm) m ty1 ext s
solveOne sm _ _ _ _ (Subtype (TApp(c,[])) (TApp(c',[]))) | bindingIf sm c == bindingIf sm c' =
  return ([],[])
solveOne sm tydefs vsm assumed ext (Subtype ty1@(TAppMeta(m,[u])) ty2@(TApp(c,args))) =
  case M.lookup (constPos c) vsm of
    Nothing -> -- we should have a variance, so just don't continue here (error will be reported from Variances)
      return ([],[])
    Just vs ->
      if length args == length vs then
        -- c is being used as a signature functor
        let (args',l) = (init args,last args) in
          do
            (assumed1,unsolved1) <- solveOne sm tydefs vsm assumed ext (Subtype (tyMeta m) (tapp (tyConst c) args'))
            (assumed2,unsolved2) <- solvePass sm tydefs vsm assumed ((ext,) <$> (propagate [last vs] [u] [l]))
            return (assumed1++assumed2,unsolved1++unsolved2)
      else
        -- c is being used as the datatype, so we will unroll it to match the functorial meta-variable
        solveOne sm tydefs vsm assumed ext (Subtype ty1 (tapp ty2 [ty2]))

--solveOne sm tydefs vsm assumed ext (Subtype (Arrow

solveOne sm tydefs vsm assumed ext s =
  do
    subst <- get
    b <- matchesElem (isLocal vsm) sm s assumed
    subst' <- get
    if b then
      (put $ M.union subst subst') >>
      return ([],[])
    else
      put subst >> -- restore original substitution
      return ([],[(ext,s)])

-- is the given const a local one?
isLocal :: VariancesMap -> Const -> Bool
isLocal vsm c = M.notMember (constPos c) vsm

matchesElem :: IsLocal -> ScopeMap -> Constraint -> [Constraint] -> Solve Bool
matchesElem islocal sm s [] = return False
matchesElem islocal sm s (s':ss) =
  do
    put M.empty -- clear the substitution
    b <- matchesConstraint islocal sm s s'
    if b then
      return True
    else
      matchesElem islocal sm s ss

matchesConstraint :: IsLocal -> ScopeMap -> Constraint -> Constraint -> Solve Bool
matchesConstraint islocal sm (Subtype ty1 ty2) (Subtype ty1' ty2') =
  (&&) <$> matches islocal sm (ty1, ty1') <*> matches islocal sm (ty2, ty2')
matchesConstraint islocal sm (Like ty1 ty2) (Like ty1' ty2') =
  (&&) <$> matches islocal sm (ty1, ty1') <*> matches islocal sm (ty2, ty2')
matchesConstraint islocal sm _ _ = return False

matches :: IsLocal -> ScopeMap -> (Ty, Ty) -> Solve Bool
matches islocal sm (ty1, ty2) =
  do
    subst <- get
    matchesInner islocal sm (substTyMeta subst ty1 , substTyMeta subst ty2) 

matchesInner :: IsLocal -> ScopeMap -> (Ty , Ty) -> Solve Bool
matchesInner islocal sm (TAppMeta(m,[]) , ty2) = addSolution islocal m ty2
matchesInner islocal sm (TApp(c,tys), TApp(c',tys')) =
  if bindingIf sm c == bindingIf sm c' then
    do
      r <- mapM (matches islocal sm) (zip tys tys')
      return (length tys == length tys' && and r)
  else
    return False
matchesInner islocal sm (TyParens _ ty,ty') = matchesInner islocal sm (ty, ty')
matchesInner islocal sm (ty,TyParens _ ty') = matchesInner islocal sm (ty, ty')
matchesInner islocal sm _ = return False
